import { Box, Button, Dialog, DialogActions, Typography, DialogContent, DialogContentText, DialogTitle, FormControlLabel, Grid, IconButton, Paper, Slide, Switch, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField, Tooltip, CircularProgress, Alert, Snackbar } from '@mui/material';
import React, { Component } from 'react';
import { styled } from '@mui/material/styles';
import DeleteIcon from '@mui/icons-material/Delete';
import gatewayService from '../services/gatewayService';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Android12Switch = styled(Switch)(({ theme }) => ({
    padding: 8,
    '& .MuiSwitch-track': {
        borderRadius: 22 / 2,
        '&:before, &:after': {
            content: '""',
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-50%)',
            width: 16,
            height: 16,
        },
        '&:before': {
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
                theme.palette.getContrastText(theme.palette.primary.main),
            )}" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z"/></svg>')`,
            left: 12,
        },
        '&:after': {
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
                theme.palette.getContrastText(theme.palette.primary.main),
            )}" d="M19,13H5V11H19V13Z" /></svg>')`,
            right: 12,
        },
    },
    '& .MuiSwitch-thumb': {
        boxShadow: 'none',
        width: 16,
        height: 16,
        margin: 2,
    },
}));

class EditGateway extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            current_device_idx: 0,
            current_device: {},
            new_device: {},
            formData: {},
            formErrors: {
                ipv4: false
            },
            deleteDialog: false,
            loaded: false
        }
    }

    openDeleteDialog = (idx, row) => {
        this.setState({
            current_device_idx: idx,
            current_device: row,
            deleteDialog: true
        });
    }

    changeInput = (e) => {
        let data = this.state.data;
        let formErrors = this.state.formErrors;

        data[e.target.name] = e.target.value;

        if (e.target.name === "ipv4_address") {
            if (!(/^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/.test(e.target.value))) {
                formErrors['ipv4'] = true;
            } else {
                formErrors['ipv4'] = false;
            }
        }

        this.setState({
            data: data,
            formErrors: formErrors
        });
    }

    changeNewDeviceInput = (e) => {
        let formData = this.state.formData;
        let formErrors = this.state.formErrors;

        formData[e.target.name] = e.target.value;

        if(e.target.name === 'uid'){
            if (!(/^[+]?\d+([.]\d+)?$/.test(e.target.value))) {
                formErrors['uid'] = true;
            } else {
                formErrors['uid'] = false;
            }
        }


        this.setState({
            formData: formData,
            formErrors: formErrors
        });
    }

    changeDeviceInput = (e, idx) => {
        let data = this.state.data;
        let devices = data.device;
        let current_device = devices[idx];

        current_device[e.target.name] = e.target.value;

        devices[idx] = current_device;
        data['device'] = devices;

        this.setState({
            data: data
        });
    }

    changeSwitch = async (status, idx) => {
        let data = this.state.data;
        let devices = data.device;
        let current_device = devices[idx];

        current_device["status"] = !status;

        devices[idx] = current_device;
        data["device"] = devices;

        this.setState({
            data: data
        })

        this.updateGateway();
    }

    addDevice = () => {

        this.setState({
            loaded: false
        });

        let data = this.state.data;
        let devices = data.device;
        let this_device = {};
        let formData = this.state.formData;


        if (this.state.formErrors.uid) {
            this.setState({
                loaded: true,
                severity: "warning",
                message: "Invalid device uid.",
                snackbar: true
            })
            return;
        }

        if (devices.length >= 10) {
            this.setState({
                loaded: true,
                severity: "warning",
                message: "Peripheral device limit is 10",
                snackbar: true
            })
            return;
        }

        if (formData.uid === "" || formData.vendor === "") {
            this.setState({
                loaded: true,
                severity: "warning",
                message: "Please fill all the fields",
                snackbar: true
            })
            return;
        }

        this_device['uid'] = formData.uid;
        this_device['vendor'] = formData.vendor;
        this_device['status'] = true;
        this_device['not_in_db'] = true;
        devices.push(this_device);

        formData['uid'] = '';
        formData['vendor'] = '';

        data.device = devices

        this.setState({
            data: data,
            loaded: true
        })
    }

    triggerDelete = (idx) => {

        let data = this.state.data;
        let devices = data.device;

        devices.splice(idx, 1);

        data['device'] = devices;

        this.setState({
            data: data,
            deleteDialog: false
        });

    }

    deleteDevice = () => {
        let data = this.state.data;
        let current_device_idx = this.state.current_device_idx;
        let devices = data.device;

        devices.splice(current_device_idx, 1);

        data["device"] = devices;

        this.setState({
            data: data
        })

        this.updateGateway();
    }

    updateGateway = async () => {

        if (this.state.formErrors.ipv4) {
            this.setState({
                snackbar: true,
                severity: "warning",
                message: "Format of the entered IPv4 Address is incorrect. Please check again"
            })

            return;
        }

        this.setState({
            loaded: false
        })

        let data = this.state.data;

        await gatewayService.updateGateway(data, data._id)
            .then(res => {
                this.setState({
                    data: data,
                    loaded: true,
                    severity: "success",
                    message: res.data.msg,
                    snackbar: true,
                    deleteDialog: false
                })
            }
            )
            .catch((error) => {
                console.log(error);
                let error_msg = "Error [" + error.response.data.error.message + "]"
                this.setState({
                    severity: "error",
                    message: error_msg,
                    snackbar: true,
                    loaded: true,
                });
            })
    }

    formSubmit = (e) => {
        this.updateGateway();

        e.preventDefault();
    }

    componentDidMount = () => {
        this.setState({
            data: this.props.data,
            loaded: true
        })
    }

    render() {
        return (
            <Box>
                <form onSubmit={(e) => this.formSubmit(e)}>
                    {this.state.loaded ?
                        <Grid container spacing={2} className="containerGrid">
                            <Grid item md={8}>
                                <TextField
                                    id="name"
                                    name="name"
                                    label="Name"
                                    type="text"
                                    size="small"
                                    className="input"
                                    value={this.state.data.name ? this.state.data.name : ""}
                                    onChange={(e) => this.changeInput(e)}
                                />
                            </Grid>
                            <Grid item md={2}>
                                <TextField
                                    id="serial_number"
                                    name="serial_number"
                                    label="Serial No."
                                    type="text"
                                    size="small"
                                    className="input"
                                    value={this.state.data.serial_number ? this.state.data.serial_number : ""}
                                    onChange={(e) => this.changeInput(e)}
                                />
                            </Grid>
                            <Grid item md={2}>
                                <TextField
                                    id="ipv4_address"
                                    name="ipv4_address"
                                    label="IPv4 Address"
                                    type="text"
                                    size="small"
                                    className="input"
                                    value={this.state.data.ipv4_address ? this.state.data.ipv4_address : ""}
                                    onChange={(e) => this.changeInput(e)}
                                />
                                {this.state.formErrors.ipv4 ?
                                    <span style={{ color: "red", fontSize: "13px", paddingLeft: "10px" }}>
                                        invalid ipv4 format
                                    </span>
                                    : ""}
                            </Grid>

                            <Grid item md={12} sx={{ marginTop: "20px" }}>
                                <Grid container spacing={2} className="subContainerGrid">
                                    <Grid item md={12} className="subContainer">
                                        <Grid container spacing={2}>
                                            <Grid item md={4}></Grid>
                                            <Grid item md={4} sx={{ borderLeft: "1px solid #1976D2", borderRight: "1px solid #7b1fa2", marginTop: "-20px", backgroundColor: "white" }}>
                                                <Typography sx={{ marginTop: "-10px", textAlign: "center", color: "#7b1fa2" }}>
                                                    Add Peripheral Device
                                                </Typography>
                                            </Grid>
                                            <Grid item md={4}></Grid>
                                            <Grid item md={2}>
                                                <TextField
                                                    id="uid"
                                                    name="uid"
                                                    label="Device ID"
                                                    type="text"
                                                    size="small"
                                                    className="input"
                                                    onChange={(e) => this.changeNewDeviceInput(e)}
                                                    value={this.state.formData.uid ? this.state.formData.uid : ""}
                                                />
                                                {this.state.formErrors.uid ?
                                                    <span style={{ color: "red", fontSize: "13px", paddingLeft: "10px" }}>
                                                        only positive whole numbers allowed
                                                    </span>
                                                    : ""}
                                            </Grid>
                                            <Grid item md={8}>
                                                <TextField
                                                    id="vendor"
                                                    name="vendor"
                                                    label="Vendor"
                                                    type="text"
                                                    size="small"
                                                    className="input"
                                                    onChange={(e) => this.changeNewDeviceInput(e)}
                                                    value={this.state.formData.vendor ? this.state.formData.vendor : ""}
                                                />
                                            </Grid>
                                            <Grid item md={2}>
                                                <Button
                                                    className="button"
                                                    variant="contained"
                                                    color="secondary"
                                                    onClick={() => this.addDevice()}
                                                >
                                                    Add Device
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>

                            {this.state.data.device ? this.state.data.device.length > 0 ?
                                <Grid item md={12}>
                                    <Grid container spacing={2} className="containerGrid">
                                        <Grid item md={12}>
                                            <TableContainer component={Paper}>
                                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                                    <TableHead>
                                                        <TableRow sx={{ backgroundColor: "#87B7E8" }}>
                                                            <TableCell sx={{ fontWeight: "bold" }}>Actions</TableCell>
                                                            <TableCell sx={{ fontWeight: "bold" }}>UID</TableCell>
                                                            <TableCell sx={{ fontWeight: "bold" }}>Vendor</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        {this.state.data.device.map((row, idx) => (
                                                            <TableRow
                                                                key={idx}
                                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                            >
                                                                <TableCell component="th" scope="row" sx={{ width: "120px" }}>
                                                                    <Tooltip title={!row.not_in_db ? "Change Status" : ""} placement="bottom">
                                                                        <FormControlLabel control={
                                                                            <Android12Switch
                                                                                checked={row.status}
                                                                                onClick={() => this.changeSwitch(row.status, idx)}
                                                                                disabled={row.not_in_db}
                                                                            />
                                                                        }
                                                                        />
                                                                    </Tooltip>
                                                                    <Tooltip title="Delete" placement='right'>
                                                                        <IconButton onClick={() => this.openDeleteDialog(idx, row)}>
                                                                            <DeleteIcon color="error" />
                                                                        </IconButton>
                                                                    </Tooltip>
                                                                </TableCell>
                                                                <TableCell component="th" scope="row" sx={{ width: "150px" }}>
                                                                    <TextField
                                                                        id="uid"
                                                                        name="uid"
                                                                        label="Device ID"
                                                                        type="number"
                                                                        size="small"
                                                                        className="input"
                                                                        value={row.uid ? row.uid : ""}
                                                                        onChange={(e) => this.changeDeviceInput(e, idx)}
                                                                    />
                                                                </TableCell>
                                                                <TableCell component="th" scope="row">
                                                                    <TextField
                                                                        id="vendor"
                                                                        name="vendor"
                                                                        label="Vendor"
                                                                        type="text"
                                                                        size="small"
                                                                        className="input"
                                                                        value={row.vendor ? row.vendor : ""}
                                                                        onChange={(e) => this.changeDeviceInput(e, idx)}
                                                                    />
                                                                </TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                :
                                <Grid item md={12} sx={{ marginBottom: "15px" }} align="center">
                                    No Peripheral Devices Found!
                                </Grid>
                                : ""}
                            <Grid item md={10}></Grid>
                            <Grid item md={2}>
                                <Button
                                    className="button"
                                    variant="contained"
                                    type="submit"
                                >
                                    Save Gateway
                                </Button>
                            </Grid>
                        </Grid>
                        :
                        <Grid container spacing={2}>
                            <Grid item md={5}></Grid>
                            <Grid item md={2} align="center">
                                <CircularProgress />
                            </Grid>
                            <Grid item md={5}></Grid>
                        </Grid>
                    }
                </form>

                {/* Delete Dialog */}
                <Dialog
                    open={this.state.deleteDialog}
                    TransitionComponent={Transition}
                    keepMounted
                >
                    <DialogTitle>Are you sure?</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            You are about to delete a peripheral device. Please confirm to continue
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" color="error" onClick={() => (this.setState({ deleteDialog: false }))}>Cancel</Button>
                        {this.state.current_device.not_in_db ?
                            <Button variant="contained" onClick={() => this.triggerDelete(this.state.current_device_idx)}>Confirm</Button>
                            :
                            <Button variant="contained" onClick={() => this.deleteDevice()}>Confirm</Button>
                        }
                    </DialogActions>
                </Dialog>

                <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => (this.setState({ snackbar: false }))} anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }} >
                    <Alert variant="filled" severity={this.state.severity}>{this.state.message}</Alert>
                </Snackbar>
            </Box>
        )
    }

}

export default EditGateway;