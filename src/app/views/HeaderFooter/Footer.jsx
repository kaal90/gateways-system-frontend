import { Typography } from '@mui/material';
import React from 'react';

const Footer = () => {
    return(
        <div style={{position:"fixed", right:0, bottom:0, textAlign:"right", width:"100%", paddingRight:"10px"}}>
            <Typography sx={{color:"#1F9A77"}}>
                Developed By Nadun Vimarshana @2023
            </Typography>
        </div>        
    )
}

export default Footer;