import React, { Component } from 'react';
import Axios from 'axios';
import * as apiRoutes from '../../apiRoutes';

class gatewayService extends Component {

    getAll = async (params) => {
        const a = new Promise((resolve, reject) => {
            Axios.get(apiRoutes.GATEWAY,
                { params: params })
                .then(res => {
                    return resolve(res);
                })
                .catch((error) => {
                    console.log("error", error);
                })
        });
        return await a;
    }

    createNew = async (data) => {

        const a = new Promise((resolve, reject) => {
            Axios.post(apiRoutes.GATEWAY, data)
                .then(res => {
                    return resolve(res);
                })
                .catch((error) => {
                    console.log("error", error);
                    return reject(error);
                })
        });
        return await a;
    }

    updateGateway = async (data, id) => {

        const a = new Promise((resolve, reject) => {
            Axios.patch(apiRoutes.GATEWAY + id, data)
                .then(res => {
                    return resolve(res);
                })
                .catch((error) => {
                    console.log("error", error);
                    return reject(error);
                })
        });
        return await a;
    }

    changeStatus = async (data, id) => {

        const a = new Promise((resolve, reject) => {
            Axios.put(apiRoutes.GATEWAY_STATUS + id, data)
                .then(res => {
                    return resolve(res);
                })
                .catch((error) => {
                    console.log("error", error);
                })
        });
        return await a;
    }

    deleteGateway = async (id) => {

        const a = new Promise((resolve, reject) => {
            Axios.delete(apiRoutes.GATEWAY + id)
                .then(res => {
                    return resolve(res);
                })
                .catch((error) => {
                    console.log("error", error);
                })
        });
        return await a;
    }

}

export default new gatewayService();