import { Grid, Typography } from '@mui/material';
import { Box } from '@mui/system';
import React from 'react';

const Header = () => {
    return (
        <Box>
            <Grid container spacing={2} style={{ backgroundColor: "#1f9a77", marginBottom:"20px" }}>
                <Grid item md={1} sm={12} xs={12} sx={{textAlign:"center", marginTop:"10px", marginBottom:"10px"}}>
                    <img alt="gateway_logo" src={process.env.PUBLIC_URL + '/logo.png'} style={{ width: "40px", marginTop:"5px", marginLeft:"20px" }}/>
                </Grid>
                {/* <Grid item md={3} sm={12} xs={0}></Grid> */}
                <Grid item md={11} sm={12} xs={12} sx={{textAlign:"center", paddingBottom:"20px"}}>
                    <Typography variant="h4" sx={{color:"white"}}>
                        Gateway Management System
                    </Typography>
                </Grid>
            </Grid>
        </Box>
    )
}

export default Header;