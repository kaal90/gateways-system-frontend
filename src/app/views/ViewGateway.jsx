import { Grid, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from '@mui/material';
import React, { Component } from 'react';

class ViewGateway extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            loaded: false
        }
    }

    componentDidMount = () => {
        this.setState({
            data: this.props.data
        });
    }

    render() {
        return (
            <Grid container spacing={2} className="containerGrid">
                <Grid item md={8}>
                    <Grid container spacing={1}>
                        <Grid item md={12} className="element">
                            IPv4 Address
                        </Grid>
                        <Grid item md={12}>
                            {this.state.data.ipv4_address}
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={4}>
                    <Grid container spacing={1}>
                        <Grid item md={12} className="element">
                            Serial Number
                        </Grid>
                        <Grid item md={12}>
                            {this.state.data.serial_number}
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={12}>
                    <Grid container spacing={1}>
                        <Grid item md={12} className="element2">
                            Associated Peripheral Devices
                        </Grid>
                        {this.state.data.device ? this.state.data.device.length > 0 ?
                            <Grid item md={12}>
                                <Grid container spacing={2} className="containerGrid">
                                    <Grid item md={12}>
                                        <TableContainer component={Paper}>
                                            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                                <TableHead>
                                                    <TableRow sx={{ backgroundColor: "#87B7E8" }}>
                                                        <TableCell sx={{ fontWeight: "bold", width: "120px" }}>UID</TableCell>
                                                        <TableCell sx={{ fontWeight: "bold", width: "400px" }}>Vendor</TableCell>
                                                        <TableCell sx={{ fontWeight: "bold", width: "120px" }}>Status</TableCell>
                                                        <TableCell sx={{ fontWeight: "bold", width: "400px" }}>Created Date</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {this.state.data.device.map((row, idx) => (
                                                        <TableRow
                                                            key={idx}
                                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                        >
                                                            <TableCell component="th" scope="row">
                                                                {row.uid}
                                                            </TableCell>
                                                            <TableCell component="th" scope="row">
                                                                {row.vendor}
                                                            </TableCell>
                                                            <TableCell component="th" scope="row">
                                                                {row.status === true ? "Online" : "Offline"}
                                                            </TableCell>
                                                            <TableCell component="th" scope="row">
                                                                {row.date.split("T")[0]}
                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </Grid>
                                </Grid>
                            </Grid>
                            :
                            <Grid item md={12} sx={{ marginBottom: "15px" }} align="center">
                                No Peripheral Devices Found!
                            </Grid>
                            : ""}
                    </Grid>
                </Grid>
            </Grid>
        )
    }
}

export default ViewGateway;