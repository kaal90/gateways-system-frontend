import { Box, Button, Grid, TextField, Typography, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, IconButton, CircularProgress, Alert, Snackbar } from '@mui/material';
import React, { Component } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import gatewayService from '../services/gatewayService';

class NewGateway extends Component {
    constructor(props) {
        super(props);
        this.state = {
            devices: [],
            formData: {},
            formErrors: {
                ipv4: false
            },
            loaded: true,
            severity: "success",
            message: "",
            snackbar: false
        }
    }

    changeInput = (e) => {
        let formData = this.state.formData;
        let formErrors = this.state.formErrors;

        formData[e.target.name] = e.target.value;

        if (e.target.name === "ipv4_address") {
            if (!(/^(?!0)(?!.*\.$)((1?\d?\d|25[0-5]|2[0-4]\d)(\.|$)){4}$/.test(e.target.value))) {
                formErrors['ipv4'] = true;
            } else {
                formErrors['ipv4'] = false;
            }
        }

        if (e.target.name === 'uid') {
            if (!(/^[+]?\d+([.]\d+)?$/.test(e.target.value))) {
                formErrors['uid'] = true;
            } else {
                formErrors['uid'] = false;
            }
        }

        this.setState({
            formData: formData,
            formErrors: formErrors
        });
    }

    addDevice = () => {
        if (this.state.formErrors.uid) {
            this.setState({
                loaded: true,
                severity: "warning",
                message: "Invalid device uid.",
                snackbar: true
            })
            return;
        }

        this.setState({
            loaded: false
        });

        let devices = this.state.devices;
        let this_device = {};
        let formData = this.state.formData;

        if (devices.length >= 10) {
            this.setState({
                loaded: true,
                severity: "warning",
                message: "Peripheral device limit is 10",
                snackbar: true
            })
            return;
        }

        if (formData.uid === "" || formData.vendor === "") {
            this.setState({
                loaded: true,
                severity: "warning",
                message: "Please fill all the fields",
                snackbar: true
            })
            return;
        }

        this_device['uid'] = formData.uid;
        this_device['vendor'] = formData.vendor;
        this_device['status'] = 1;
        devices.push(this_device);

        formData['uid'] = '';
        formData['vendor'] = '';

        this.setState({
            devices: devices,
            loaded: true
        })
    }

    triggerDelete = (idx) => {

        let devices = this.state.devices;

        devices.splice(idx, 1);

        this.setState({
            devices: devices
        });

    }

    formSubmit = async (e) => {
        e.preventDefault();

        let formData = this.state.formData;
        let devices = this.state.devices;

        let newData = {};

        newData['name'] = formData.name;
        newData['serial_number'] = formData.serial_number;
        newData['ipv4_address'] = formData.ipv4_address;
        newData['device'] = devices;

        if (this.state.formErrors.ipv4) {
            this.setState({
                snackbar: true,
                severity: "warning",
                message: "Format of the entered IPv4 Address is incorrect. Please check again"
            })

            return;
        }

        await gatewayService.createNew(newData)
            .then(res => {
                this.setState({
                    severity: "success",
                    message: res.data.msg,
                    snackbar: true
                });
                formData['name'] = '';
                formData['serial_number'] = '';
                formData['ipv4_address'] = '';
                devices.length = 0;
            })
            .catch(error => {
                console.log(error);
                let error_msg = "Error [" + error.response.data.error.message + "]"
                this.setState({
                    severity: "error",
                    message: error_msg,
                    snackbar: true
                });
            })

        this.setState({
            formData: formData,
            devices: devices,
            loaded: true
        })
    }

    render() {
        return (
            <Box>
                <form onSubmit={(e) => this.formSubmit(e)}>
                    <Grid container spacing={2} className="containerGrid">
                        <Grid item md={8} xs={12}>
                            <TextField
                                id="name"
                                name="name"
                                label="Name"
                                type="text"
                                size="small"
                                className="input"
                                onChange={(e) => this.changeInput(e)}
                                value={this.state.formData.name ? this.state.formData.name : ""}
                            />
                        </Grid>
                        <Grid item md={2} xs={12}>
                            <TextField
                                id="serial_number"
                                name="serial_number"
                                label="Serial No."
                                type="text"
                                size="small"
                                className="input"
                                onChange={(e) => this.changeInput(e)}
                                value={this.state.formData.serial_number ? this.state.formData.serial_number : ""}
                            />
                        </Grid>
                        <Grid item md={2} xs={12}>
                            <TextField
                                id="ipv4_address"
                                name="ipv4_address"
                                label="IPv4 Address"
                                type="text"
                                size="small"
                                className="input"
                                onChange={(e) => this.changeInput(e)}
                                value={this.state.formData.ipv4_address ? this.state.formData.ipv4_address : ""}
                            />
                            {this.state.formErrors.ipv4 ?
                                <span style={{ color: "red", fontSize: "13px", paddingLeft: "10px" }}>
                                    invalid ipv4 format
                                </span>
                                : ""}
                        </Grid>
                        <Grid item md={12} xs={12} sx={{ marginTop: "20px" }}>
                            <Grid container spacing={2} className="subContainerGrid">
                                <Grid item md={12} xs={12} className="subContainer">
                                    <Grid container spacing={2}>
                                        <Grid item md={4} xs={4}></Grid>
                                        <Grid item md={4} xs={4} sx={{ borderLeft: "1px solid #1976D2", borderRight: "1px solid #7b1fa2", marginTop: "-20px", backgroundColor: "white" }}>
                                            <Typography sx={{ marginTop: "-10px", textAlign: "center", color: "#7b1fa2" }}>
                                                Add Peripheral Device
                                            </Typography>
                                        </Grid>
                                        <Grid item md={4} xs={4}></Grid>
                                        <Grid item md={2} xs={12}>
                                            <TextField
                                                id="uid"
                                                name="uid"
                                                label="Device ID"
                                                type="text"
                                                size="small"
                                                className="input"
                                                onChange={(e) => this.changeInput(e)}
                                                value={this.state.formData.uid ? this.state.formData.uid : ""}
                                            />
                                            {this.state.formErrors.uid ?
                                                <span style={{ color: "red", fontSize: "13px", paddingLeft: "10px" }}>
                                                    only positive whole numbers allowed
                                                </span>
                                                : ""}
                                        </Grid>
                                        <Grid item md={8} xs={12}>
                                            <TextField
                                                id="vendor"
                                                name="vendor"
                                                label="Vendor"
                                                type="text"
                                                size="small"
                                                className="input"
                                                onChange={(e) => this.changeInput(e)}
                                                value={this.state.formData.vendor ? this.state.formData.vendor : ""}
                                            />
                                        </Grid>
                                        <Grid item md={2} xs={12}>
                                            <Button
                                                className="button"
                                                variant="contained"
                                                color="secondary"
                                                onClick={() => this.addDevice()}
                                            >
                                                Add Device
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item md={12} xs={12}>
                            <Grid container spacing={1}>
                                {this.state.loaded ?
                                    <Grid item md={12} xs={12}>
                                        <Grid container spacing={2} className="containerGrid">
                                            {this.state.devices.length > 0 ?
                                                <Grid item md={12}>
                                                    <TableContainer component={Paper}>
                                                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                                            <TableHead>
                                                                <TableRow sx={{ backgroundColor: "#87B7E8" }}>
                                                                    <TableCell sx={{ fontWeight: "bold", width: "170px" }}>Actions</TableCell>
                                                                    <TableCell sx={{ fontWeight: "bold", width: "120px" }}>UID</TableCell>
                                                                    <TableCell sx={{ fontWeight: "bold", width: "400px" }}>Vendor</TableCell>
                                                                </TableRow>
                                                            </TableHead>
                                                            <TableBody>
                                                                {this.state.devices.map((row, idx) => (
                                                                    <TableRow
                                                                        key={idx}
                                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                                    >
                                                                        <TableCell component="th" scope="row">
                                                                            <Tooltip title="Delete">
                                                                                <IconButton onClick={() => this.triggerDelete(idx)}>
                                                                                    <DeleteIcon color="error" />
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                        </TableCell>
                                                                        <TableCell component="th" scope="row">
                                                                            {row.uid}
                                                                        </TableCell>
                                                                        <TableCell component="th" scope="row">
                                                                            {row.vendor}
                                                                        </TableCell>
                                                                    </TableRow>
                                                                ))}
                                                            </TableBody>
                                                        </Table>
                                                    </TableContainer>
                                                </Grid>
                                                :
                                                <Grid item md={12} xs={12} sx={{ marginBottom: "15px" }} align="center">
                                                    No Peripheral Devices Found!
                                                </Grid>
                                            }
                                        </Grid>
                                    </Grid>
                                    :
                                    <Grid item md={12} xs={12}>
                                        <Grid container spacing={2}>
                                            <Grid item md={5} xs={5}></Grid>
                                            <Grid item md={2} xs={2} align="center">
                                                <CircularProgress />
                                            </Grid>
                                            <Grid item md={5} xs={5}></Grid>
                                        </Grid>
                                    </Grid>
                                }
                            </Grid>
                        </Grid>
                        <Grid item md={9} xs={12}></Grid>
                        <Grid item md={3} xs={12}>
                            <Button
                                className="button"
                                variant="contained"
                                type="sumbit"
                            >
                                Add Gateway
                            </Button>
                        </Grid>
                    </Grid>
                </form>

                <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => (this.setState({ snackbar: false }))} anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }} >
                    <Alert variant="filled" severity={this.state.severity}>{this.state.message}</Alert>
                </Snackbar>
            </Box>
        )
    }

}

export default NewGateway;