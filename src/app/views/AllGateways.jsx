import React, { Component } from 'react';

//mui components
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { Alert, AppBar, Autocomplete, Button, Chip, CircularProgress, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControlLabel, IconButton, Slide, Snackbar, TextField, Toolbar, Tooltip, Typography } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import VisibilityIcon from '@mui/icons-material/Visibility';
import CreateIcon from '@mui/icons-material/Create';
import { styled } from '@mui/material/styles';
import Switch from '@mui/material/Switch';
import CancelIcon from '@mui/icons-material/Cancel';
import DeleteIcon from '@mui/icons-material/Delete';
import RefreshIcon from '@mui/icons-material/Refresh';

//styling
import '.././styles/main.css';

import Header from './HeaderFooter/Header';
import NewGateway from './NewGateway';
import ViewGateway from './ViewGateway';
import EditGateway from './EditGateway';

import gatewayService from '../services/gatewayService';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

const Android12Switch = styled(Switch)(({ theme }) => ({
    padding: 8,
    '& .MuiSwitch-track': {
        borderRadius: 22 / 2,
        '&:before, &:after': {
            content: '""',
            position: 'absolute',
            top: '50%',
            transform: 'translateY(-50%)',
            width: 16,
            height: 16,
        },
        '&:before': {
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
                theme.palette.getContrastText(theme.palette.primary.main),
            )}" d="M21,7L9,19L3.5,13.5L4.91,12.09L9,16.17L19.59,5.59L21,7Z"/></svg>')`,
            left: 12,
        },
        '&:after': {
            backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="16" width="16" viewBox="0 0 24 24"><path fill="${encodeURIComponent(
                theme.palette.getContrastText(theme.palette.primary.main),
            )}" d="M19,13H5V11H19V13Z" /></svg>')`,
            right: 12,
        },
    },
    '& .MuiSwitch-thumb': {
        boxShadow: 'none',
        width: 16,
        height: 16,
        margin: 2,
    },
}));

class AllGateways extends Component {
    constructor(props) {
        super(props);
        this.state = {
            gateways: [],
            formData: {},
            filterData: {},
            statuses: [
                "Active",
                "Inactive"
            ],
            newGatewayDialog: false,
            viewGatewayDialog: false,
            editGatewayDialog: false,
            deleteDialog: false,
            loaded: false,
            current_gateway: {},
            severity: "success",
            message: "",
            snackbar: false
        }
    }

    openNewGatewayDialog = () => {
        this.setState({
            newGatewayDialog: true
        });
    }

    openViewGatewayDialog = (row) => {
        this.setState({
            current_gateway: row,
            viewGatewayDialog: true
        });
    }

    openEditGatewayDialog = (row) => {
        this.setState({
            current_gateway: row,
            editGatewayDialog: true
        });
    }

    openDeleteDialog = (row) => {
        this.setState({
            current_gateway: row,
            deleteDialog: true
        });
    }

    closeDialog = () => {
        this.setState({
            editGatewayDialog: false,
            viewGatewayDialog: false,
            newGatewayDialog: false
        });

        this.loadData();
    }

    changeInput = (e) => {
        let formData = this.state.formData;

        formData[e.target.name] = e.target.value;

        this.setState({
            formData: formData
        });
    }

    changeDropdown = (name, value) => {
        let formData = this.state.formData;
        if (value === '' || value === null) {
            formData[name] = '';
        }

        formData[name] = value;

        this.setState({
            formData: formData
        });
    }

    deleteGateway = async () => {
        let current_gateway = this.state.current_gateway;

        await gatewayService.deleteGateway(current_gateway._id)
            .then(res => {
                this.setState({
                    severity: "success",
                    message: res.data.msg,
                    snackbar: true
                })
            },
                this.loadData()
            )
            .catch((error) => {
                console.log(error);
                this.setState({
                    severity: "error",
                    message: "Error on Updating Status!",
                    snackbar: true,
                });
            })
    }

    changeSwitch = async (status, idx) => {
        let gateways = this.state.gateways;
        let current_gateway = gateways[idx];

        if (status === true) {
            status = false;
        } else {
            status = true;
        }

        current_gateway["status"] = status;

        gateways[idx] = current_gateway;

        let data = {
            status: status
        }

        await gatewayService.changeStatus(data, current_gateway._id)
            .then(res => {
                this.setState({
                    gateways: gateways,
                    severity: "success",
                    message: res.data.msg,
                    snackbar: true
                })
            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    severity: "error",
                    message: "Error on Updating Status!",
                    snackbar: true,
                });
            })
    }

    refreshPage = () => {
        
        let formData = this.state.formData;
        let filterData = this.state.filterData;
        formData['name'] = '';
        filterData['name'] = '';
        formData['status'] = null;
        filterData['status'] = null;
        formData['serial_number'] = '';
        filterData['serial_number'] = '';

        this.setState({
            formData: formData,
            filterData:filterData
        })

        this.loadData();
    }

    loadDataOnSubmit = (e) => {
        this.loadData();
        e.preventDefault();
    }

    loadData = async () => {

        let formData = this.state.formData;
        let filterData = {};

        filterData["name"] = formData.name;
        filterData["serial_number"] = formData.serial_number;
        filterData["no_of_devices"] = formData.no_of_devices;

        if (formData.status === "Active") {
            filterData["status"] = true;
        } else if (formData.status === "Inactive") {
            filterData["status"] = false;
        }
        this.setState({
            loaded: false,
        });

        await gatewayService.getAll(filterData)
            .then(res => {
                this.setState({
                    gateways: res.data,
                    loaded: true,
                    formData: formData
                });
            })
            .catch((error) => {
                console.log(error);
                this.setState({
                    severity: "error",
                    message: "Error on Getting Data!",
                    snackbar: true,
                    loaded: true
                });
            })
    }

    componentDidMount = () => {
        this.loadData();
    }

    render() {
        return (
            <Box sx={{ flexGrow: 1 }}>
                <Grid container spacing={2}>
                    <Grid item md={12}><Header /></Grid>
                </Grid>
                <Grid container spacing={2} className="containerGrid">
                <Grid item md={8} xs={11}></Grid>
                    <Grid item md={1} xs={1} style={{paddingRight:"20px"}} align="right">
                        <Tooltip title="Refresh">
                            <IconButton onClick={() => this.refreshPage()} >
                                <RefreshIcon color="secondary" />
                            </IconButton>
                        </Tooltip>
                    </Grid>                    
                    <Grid item md={3} xs={12}>
                        <Button
                            className="button"
                            variant="contained"
                            color="success"
                            onClick={() => this.openNewGatewayDialog()}
                        >
                            Add New Gateway
                        </Button>
                    </Grid>
                </Grid>

                <form onSubmit={(e) => this.loadDataOnSubmit(e)}>
                    <Grid container spacing={2} className="containerGrid">
                        <Grid item md={5} xs={12}>
                            <TextField
                                className="input"
                                id="name"
                                name="name"
                                label="Name"
                                variant="outlined"
                                size="small"
                                onChange={(e) => this.changeInput(e)}
                                value={this.state.formData.name ? this.state.formData.name : ""}
                            />
                        </Grid>
                        <Grid item md={2} xs={12}>
                            <Autocomplete
                                className="input"
                                id="status"
                                name="status"
                                options={this.state.statuses}
                                value={
                                    this.state.formData.status ? this.state.formData.status : ""
                                }
                                onChange={(event, value) => this.changeDropdown('status', value)}
                                renderInput={
                                    (params) => (
                                        <TextField
                                            {...params}
                                            label="Status"
                                            variant="outlined"
                                            value={
                                                this.state.formData.status ? this.state.formData.status : ""
                                            }
                                            fullWidth={true}
                                            size="small"
                                        />
                                    )
                                }
                            />
                        </Grid>
                        <Grid item md={3} xs={12}>
                            <TextField
                                className="input"
                                id="serial_number"
                                name="serial_number"
                                label="Serial Number"
                                variant="outlined"
                                size="small"
                                onChange={(e) => this.changeInput(e)}
                                value={this.state.formData.serial_number ? this.state.formData.serial_number : ""}
                            />
                        </Grid>
                        <Grid item md={2} xs={12}>
                            <Button
                                className="button"
                                variant="contained"
                                type="submit"
                                disabled={!this.state.loaded}
                            >
                                Search
                            </Button>
                        </Grid>
                    </Grid>
                </form>
                <Grid container spacing={2} className="containerGrid">
                    {this.state.loaded ?
                        <Grid item md={12}>
                            {this.state.gateways.length > 0 ?
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                        <TableHead>
                                            <TableRow sx={{ backgroundColor: "#87B7E8" }}>
                                                <TableCell sx={{ fontWeight: "bold", width: "300px" }}>Actions</TableCell>
                                                <TableCell sx={{ fontWeight: "bold", width: "300px" }}>Name</TableCell>
                                                <TableCell sx={{ fontWeight: "bold", width: "120px" }}>Serial Number</TableCell>
                                                <TableCell sx={{ fontWeight: "bold", width: "120px" }}>IPv4 Address</TableCell>
                                                <TableCell sx={{ fontWeight: "bold", width: "120px" }}>No. of Devices</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {this.state.gateways.map((row, idx) => (
                                                <TableRow
                                                    key={idx}
                                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                >
                                                    <TableCell component="th" scope="row">
                                                        <Tooltip title="View">
                                                            <IconButton onClick={() => this.openViewGatewayDialog(row)} >
                                                                <VisibilityIcon color="primary" />
                                                            </IconButton>
                                                        </Tooltip>
                                                        <Tooltip title="Edit">
                                                            <IconButton onClick={() => this.openEditGatewayDialog(row)} >
                                                                <CreateIcon color="primary" />
                                                            </IconButton>
                                                        </Tooltip>
                                                        <Tooltip title="Delete" placement='right'>
                                                            <IconButton onClick={() => this.openDeleteDialog(row)}>
                                                                <DeleteIcon color="error" />
                                                            </IconButton>
                                                        </Tooltip>
                                                        <Tooltip title="Change Status" style={{ marginLeft: "2px" }}>
                                                            <FormControlLabel
                                                                control={
                                                                    <Android12Switch
                                                                        checked={row.status}
                                                                        onClick={() => this.changeSwitch(row.status, idx)}
                                                                    />
                                                                }
                                                            />
                                                        </Tooltip>
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {row.name}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {row.serial_number}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row">
                                                        {row.ipv4_address}
                                                    </TableCell>
                                                    <TableCell component="th" scope="row" align="center">
                                                        <Chip label={row.device.length} color="primary" variant="outlined" />
                                                    </TableCell>
                                                </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                                :
                                <Grid container>
                                    <Grid item md={2}></Grid>
                                    <Grid item md={8} sx={{ backgroundColor: "lightgray", padding: "10px", borderRadius: "20px 20px 20px 20px" }} align="center">
                                        No Items have been loaded!
                                    </Grid>
                                    <Grid item md={2}></Grid>
                                </Grid>
                            }
                        </Grid>
                        :
                        <Grid item md={12}>
                            <Grid container spacing={2}>
                                <Grid item md={5}></Grid>
                                <Grid item md={2} align="center">
                                    <CircularProgress />
                                </Grid>
                                <Grid item md={5}></Grid>
                            </Grid>
                        </Grid>
                    }
                </Grid>
                {/* New Gateway */}
                <Dialog fullScreen open={this.state.newGatewayDialog} TransitionComponent={Transition}>
                    <AppBar sx={{ position: 'relative', backgroundColor: "#1F9A77" }}>
                        <Toolbar>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                                Create New Gateway
                            </Typography>
                            <IconButton
                                edge="start"
                                color="inherit"
                                onClick={() => (this.closeDialog())}
                                aria-label="close"
                            >
                                <CancelIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <NewGateway />
                </Dialog>

                {/* View Gateway */}
                <Dialog maxWidth="md" fullWidth={true} onClose={() => { this.setState({ viewGatewayDialog: false }) }} open={this.state.viewGatewayDialog}>
                    <AppBar sx={{ position: 'relative', backgroundColor: "#1F9A77" }}>
                        <Toolbar>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                                {this.state.current_gateway.name}
                            </Typography>
                            <IconButton
                                edge="start"
                                color="inherit"
                                onClick={() => (this.setState({ viewGatewayDialog: false }))}
                                aria-label="close"
                            >
                                <CancelIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <ViewGateway data={this.state.current_gateway} />
                </Dialog>

                {/* Edit Gateway */}
                <Dialog fullScreen open={this.state.editGatewayDialog} TransitionComponent={Transition}>
                    <AppBar sx={{ position: 'relative', backgroundColor: "#1F9A77" }}>
                        <Toolbar>
                            <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                                Edit Gateway
                            </Typography>
                            <IconButton
                                edge="start"
                                color="inherit"
                                onClick={() => (this.closeDialog())}
                                aria-label="close"
                            >
                                <CancelIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <EditGateway data={this.state.current_gateway} />
                </Dialog>

                {/* Delete Dialog */}
                <Dialog
                    open={this.state.deleteDialog}
                    TransitionComponent={Transition}
                    keepMounted
                >
                    <DialogTitle>Are you sure?</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-slide-description">
                            You are about to delete a gateway. Please confirm to continue
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <form onSubmit={() => this.deleteGateway()}>
                            <Grid container className="containerGrid">
                                <Grid item md={6}>
                                    <Button variant="contained" color="error" onClick={() => (this.setState({ deleteDialog: false }))}>Cancel</Button>
                                </Grid>
                                <Grid item md={6}>
                                    <Button variant="contained" type="submit">Confirm</Button>
                                </Grid>
                            </Grid>
                        </form>
                    </DialogActions>
                </Dialog>

                <Snackbar open={this.state.snackbar} autoHideDuration={3000} onClose={() => (this.setState({ snackbar: false }))} anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }} >
                    <Alert variant="filled" severity={this.state.severity}>{this.state.message}</Alert>
                </Snackbar>
            </Box>
        )
    }
}

export default AllGateways;