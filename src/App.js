import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';

//views
import AllGateways from './app/views/AllGateways';
import NotFound from './app/views/NotFound/NotFound';

class App extends Component {
  render() {
    return (
      <Router>
        <Routes>
          <Route path="/" element={<AllGateways />} />

          {/* 404 page */}
          <Route path="/NotFound" element={<NotFound />} />
          <Route path="*" element={<Navigate to="/NotFound" replace />} />
        </Routes>
      </Router>
    )
  }
}

export default App